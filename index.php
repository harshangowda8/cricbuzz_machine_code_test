<?php

$skip = 0;
$limit = 500;

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://api.coinstats.app/public/v1/coins?skip='.$skip.'&limit='.$limit,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'GET',
));

$response = curl_exec($curl);

?>

<!DOCTYPE html>
<html lang="en">
<head>
  
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- import jquery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <!-- import bootstrap -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <!-- import styles for datatable -->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/dataTables.bootstrap5.min.css">
  <!-- import datatable js -->
  <script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.13.1/js/dataTables.bootstrap5.min.js"></script>
  <script src="https://cdn.datatables.net/select/1.5.0/js/dataTables.select.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" crossorigin="anonymous" referrerpolicy="no-referrer" />

</head>
<body class="container-fluid">
    <header class="m-2 w-100 text-center"><h1>Crypto Listings</h1></header>
    <main class="mx-auto">
         <table class="table" id="criptoes">
        <thead>
            <tr>
                <td>Rank</td>
                <td>Name</td>
                <td>Price</td>
                <td>Market Cap</td>
                <td>Supply</td>
                <td>Volume</td>
                <td>Price Change(1 Hour)</td>
                <td>Price Change(1 Day)</td>
                <td>Price Change(1 Week)</td>
                <td>Favourite</td>

            </tr>
        </thead>
    </table>
       
    </main>
    <footer class="m-2">
        This is footer
    </footer>
    <script>
        $(document).ready(function () {

            var data = <?php echo $response; ?>['coins'];

            /* Create an array with the values of all the checkboxes in a column */
            $.fn.dataTable.ext.order['dom-checkbox'] = function (settings, col) {
                return this.api()
                    .column(col, { order: 'index' })
                    .nodes()
                    .map(function (td, i) {
                        return $('input', td).prop('checked') ? '1' : '0';
                    });
            };

            var datatable = $('#criptoes').DataTable({
                "data": data,
                "columns": [
                    { "data": "rank" },
                    { "data": "name" },
                    { "data": "price" },
                    { "data": "marketCap" },
                    { "data": "availableSupply" },
                    { "data": "volume" },
                    { "data": "priceChange1h" },
                    { "data": "priceChange1d" },
                    { "data": "priceChange1w" },
                    { "data": "favourite","render" : () => { return "<input type='checkbox' class='fav-checkbox'/>"} },
                ],
                "columnDefs": [
                    {
                        "targets": [0,1,2,3,4,5,6,6,7,8,9],
                        "defaultContent": ""
                    },
                    {   
                        "targets" : 9, 
                        "sSortDataType": "dom-checkbox" 
                    },
                    {   
                        "orderable": false, 
                        "targets": [0,1,4,5]
                    }
                ],
            });



             $('#criptoes').on("change", "tbody input.fav-checkbox[type='checkbox']", function(){
                if (($('input.fav-checkbox[type="checkbox"]:checked').length > 3)){
                    $(this).prop('checked', false)
                    alert("You can select only three favourite once, please remove any favourite add new favourite");
                } 
            });



        });
    </script>
</body>
</html>